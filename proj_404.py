# Working with audio signals
from scipy.io import wavfile
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import scipy.fftpack as FFT
import MFCC
import mfcc_features as mf


# Class Signal that takes in rate of samples (Hz) and wave samples. 
# Functions: plot() will plot the wave
class Signal:
    def __init__(self, rate, wave):
        self.rate = rate
        self.wave = wave
        self.time = len(wave) / float(rate)
        self.timestep = self.time / self.rate
        self.mfcc = MFCC.extract(wave)
        self.num_chunks, self.freqs = self.mfcc.shape[0], MFCC.CF[1:11]
    
    def plot(self):
        num_samples = self.wave.shape[0]
        total_time = num_samples / float(self.rate)
        time_steps = [i*(1./float(self.rate)) for i in xrange(num_samples)]
        plt.plot(time_steps, self.wave)
        plt.title('Time-Series Plot')
        plt.show()
    
    def plot_middle_mfcc(self):
        plt.plot(self.freqs,self.mfcc[self.num_chunks/2])
        plt.title('FFT Coefficients')
        plt.show()
        
    def plot_mfcc_id(self):
        MFCC.show_MFCC(self.mfcc)
        plt.show()
    
                
    

# Example signal to test the Signal class object
def ex_sig():
    time = 0.2
    rate = 44100
    num_samples = time * rate
    wave = np.random.randint(-32767,32767, num_samples)
    
    sig = Signal(rate, wave)
    sig.plot()

# plot bilbao signal
def bilb():
    rate, wave = wavfile.read('bilbao_clip_a.wav')
    wave = np.average(wave,axis=1)
    bilbao = Signal(rate, wave)
    # Plot the waveform
    #bilbao.plot()
    
    # Plot the FFT coefficients of the clip
    #bilbao.plot_fft()
    
    # From the total time of the clip, we want the get the fingerprint of each chunk
    num_time_chunks = 50
    time = bilbao.time
    chunk_step = time / float(num_time_chunks)
    freqs = bilbao.freqs
    for i in xrange(num_time_chunks):
        start = i * chunk_step
        wave_chunk = wave[start:start+chunk_step]
        chunk_sig = Signal(rate, wave_chunk)
        chunk_coeff, chunk_freqs = chunk_sig.get_fft()
        
    
    
    return bilbao
    
    
